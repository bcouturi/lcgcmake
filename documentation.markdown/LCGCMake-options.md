| Option                | Value(s)         | Description  |
| --------------------- |:----------------:| ------------ |
|   LCG_SAFE_INSTALL    |      ON/OFF      | ensure that no overwites occurs at the installation area of packages; |
|   LCG_IGNORE          |     \<string\>     | list of packages to be ignored from LCG_INSTALL_PREFIX (';' separated); |
|   LCG_INSTALL_PREFIX  |      \<path\>      | existing LCG installation path prefix; |
|   LCG_TARBALL_INSTALL |      ON/OFF      | turn ON/OFF creation/installation of tarballs; |
|   VALIDATION          |      ON/OFF      | turn ON/OFF validation options for LCGSoft tests; |
|   PDFsets             |     \<string\>     | name of PDF sets to be downloaded. |
|   CMAKE_BUILD_TYPE    |    'Release'     | choose the type of build. |
|                       |     'Debug'      | |
|                       |   'MinSizeRel'   | |
|                       | 'RelWithDebInfo' | |
