#--- General parameters ----------------------------------------------------------------------------
set(Python_cmd ${Python_home}/bin/python)

#---Forward declarations----------------------------------------------------------------------------
LCGPackage_set_home(pythia8)
LCGPackage_set_home(numpy)

#---ROOT--------------------------------------------------------------------------------------------
string(REGEX MATCH "[0-9]+[.][0-9]+[.][0-9]+" ROOT_author_version "${ROOT_native_version}")

LCGPackage_Add(
    ROOT
    IF <VERSION> MATCHES "^v.*-patches|HEAD" THEN
      GIT_REPOSITORY http://root.cern.ch/git/root.git GIT_TAG <VERSION>
      UPDATE_COMMAND <VOID>
    ELSE
      URL http://root.cern.ch/download/root_v${ROOT_author_version}.source.tar.gz
    ENDIF
    CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
               -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
               -Dpython=ON
               IF Python_native_version VERSION_GREATER 3 THEN
                 -Dpython3=ON
               ENDIF
               -Dbuiltin_pcre=ON
               -Dcintex=ON
               IF DEFINED Davix_native_version THEN
                 -Ddavix=OFF
               ENDIF
               -Dexceptions=ON
               -Dexplicitlink=ON
               -Dfftw3=ON
               -Dgdml=ON
               -Dgsl_shared=ON
               -Dhttp=ON
               -Dkrb5=ON
               -Dgenvector=ON
               IF <VERSION> MATCHES "^v6-|^6[.]" AND NOT LCG_TARGET MATCHES mac THEN
                 -Dvc=ON
               ENDIF
               -Dldap=ON
               -Dmathmore=ON
               -Dmemstat=ON
               -Dminuit2=ON
               -Dmysql=OFF
               -Dodbc=OFF
               -Dopengl=ON
               -Dpgsql=OFF
               -Dqtgsi=ON
               -Dreflex=ON
               -Dr=OFF
               -Droofit=ON
               -Drfio=OFF
               -Dssl=ON
               -Dtable=ON
               -Dunuran=ON
               -Dxft=ON
               -Dxml=ON
               -Dxrootd=OFF
               -DCINTMAXSTRUCT=36000
               -DCINTMAXTYPEDEF=36000
               -DCINTLONGLINE=4096
               IF LCG_CPP11 THEN
                   -Dc++11=ON
                   -Dcxx11=ON
               ENDIF
               IF LCG_CPP14 OR LCG_CPP1Y THEN
                   -Dc++14=ON
                   -Dcxx14=ON
               ENDIF

               IF LCG_TARGET MATCHES x86_64-slc THEN
                   -Dcastor=ON
                   -Ddcache=ON
                   -Dgfal=ON -DGFAL_DIR=${gfal_home}
                             -DSRM_IFCE_DIR=${srm_ifce_home}
               ENDIF
               IF LCG_TARGET MATCHES slc OR LCG_TARGET MATCHES centos THEN
                   -Doracle=ON -DORACLE_HOME=${oracle_home}
                   -Dqt=ON
               ENDIF
               IF LCG_TARGET MATCHES "mac" AND NOT LCG_TARGET MATCHES "mac109|mac1010" THEN
                   -Dqt=ON
               ENDIF
    DEPENDS Python fftw graphviz GSL mysql xrootd R numpy
            IF DEFINED Davix_native_version THEN
                Davix
            ENDIF
            IF LCG_TARGET MATCHES x86_64-slc THEN
                CASTOR dcap gfal srm_ifce
            ENDIF
            IF LCG_TARGET MATCHES slc OR LCG_TARGET MATCHES centos THEN
                oracle Qt
            ENDIF
            IF LCG_TARGET MATCHES "mac" AND NOT LCG_TARGET MATCHES "mac109|mac1010" THEN
                Qt
            ENDIF
)

#---cmaketool----------------------------------------------------------------------------------------
LCGPackage_Add(
  cmaketools
  GIT_REPOSITORY https://gitlab.cern.ch/sft/cmaketools.git GIT_TAG tags/<VERSION>
  UPDATE_COMMAND <VOID>
#  SVN_REPOSITORY  http://svn.cern.ch/guest/cmaketools/tags/${cmaketools_native_version} --quiet
  UPDATE_COMMAND <VOID>
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
)

#---RELAX---------------------------------------------------------------------------------------------
LCGPackage_Add(
  RELAX
  SVN_REPOSITORY http://svn.cern.ch/guest/relax/tags/${RELAX_native_version}/relax --quiet
  UPDATE_COMMAND <VOID>
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DCMAKE_MODULE_PATH=${cmaketools_home}/modules
             -DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS}
             -DROOTSYS=${ROOT_home}
             IF DEFINED GCCXML_native_version THEN
               -DGCCXML=${GCCXML_home}/bin/gccxml
             ENDIF
             -DCLHEP_ROOT_DIR=${CLHEP_home}
             -DHEPMC_ROOT_DIR=${HepMC_home}
             -DGSL_ROOT_DIR=${GSL_home}
             -DHEPPDT_ROOT_DIR=${HepPDT_home}
  BUILD_COMMAND ${MAKE} ROOTSYS=${ROOT_home}
  DEPENDS cmaketools ROOT
          IF DEFINED GCCXML_native_version THEN
            GCCXML
          ENDIF
          CLHEP HepMC HepPDT GSL
)

#---CMT-----------------------------------------------------------------------------------------------
LCGPackage_Add(
  cmt
  URL http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/CMT-${cmt_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>/CMT
          COMMAND ${CMAKE_COMMAND} -E chdir <INSTALL_DIR>/CMT/${cmt_native_version}/mgr ./INSTALL
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)

#---LCGCMT--------------------------------------------------------------------------------------------
LCGPackage_Add(
  LCGCMT
  URL http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/LCGCMT-cmake_root6.tar.gz
  UPDATE_COMMAND ${PYTHON} ${CMAKE_SOURCE_DIR}/cmake/scripts/create_LCGCMT.py ${CMAKE_BINARY_DIR}/dependencies.json <SOURCE_DIR>/LCG_Configuration/cmt/requirements
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>/..
  DEPENDS Python
)

# environment variables needed for CMT based builds
#set( LCGCMT_pos "/afs/cern.ch/sw/lcg/app/releases/LCGCMT/LCGCMT_66")
set(LCGCMT_pos "${LCGCMT_home}/.." )
set(CMTROOT ${cmt_home}/CMT/${cmt_native_version})

set(CMT_make_cmd
    ${env_cmd} CMTSITE=CERN CMTCONFIG=${LCG_system} CMTPATH=<SOURCE_DIR>:${LCGCMT_pos}
               CMTROOT=${CMTROOT} CMTBIN=${CMAKE_SYSTEM_NAME}-${CMAKE_SYSTEM_PROCESSOR}
	       IF LCG_CPP11 THEN -DCMAKE_CXX_FLAGS=-std=c++11 ENDIF
	       IF LCG_CPP14 THEN -DCMAKE_CXX_FLAGS=-std=c++14 ENDIF
               ${MAKE} -j1 -f ${CMAKE_SOURCE_DIR}/cmake/utils/Makefile-cmt.mk
                       -C <SOURCE_DIR>
                       CMT=${CMTROOT}/${CMAKE_SYSTEM_NAME}-${CMAKE_SYSTEM_PROCESSOR}/cmt)

#---CORAL----------------------------------------------------------------------------------------------
set(POST_INSTALL_old ${POST_INSTALL})
set(POST_INSTALL OFF)
LCGPackage_add(
  CORAL
  IF CORAL_native_version MATCHES HEAD THEN
    ###SVN_REPOSITORY http://svn.cern.ch/guest/lcgcoral/coral/trunk --quiet
    SVN_REPOSITORY svn+ssh://svn.cern.ch/reps/lcgcoral/coral/trunk --quiet
  ELSE
    ###URL http://service-spi.web.cern.ch/service-spi/external/tarFiles/CORAL-${CORAL_native_version}.tar.gz # OLD
    URL http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/CORAL-${CORAL_native_version}.tar.gz
  ENDIF
  CMAKE_GENERATOR Ninja
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
             -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
             -DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS}
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DCMAKE_VERBOSE_MAKEFILE=ON
             -DBINARY_TAG=${BINARY_TAG}
             -DLCG_VERSION=${LCG_VERSION}
             -DLCG_releases_base=${CMAKE_INSTALL_PREFIX}
             ${Boost_extra_configuration}
             ###-DCMAKE_USE_CCACHE=ON # Can also set it via environment variable
  DEPENDS ninja ccache Boost CppUnit expat Frontier_Client libaio mysql oracle Python QMtest XercesC
  IF NOT BINARY_TAG MATCHES "mac" THEN
    sqlite
  ENDIF
  IF NOT BINARY_TAG MATCHES "mac" AND NOT BINARY_TAG MATCHES "clang" THEN
    gperftools
  ENDIF
  IF NOT BINARY_TAG MATCHES "mac" AND NOT BINARY_TAG MATCHES "clang" AND NOT BINARY_TAG MATCHES "gcc62" THEN
    igprof libunwind
  ENDIF
  IF NOT BINARY_TAG MATCHES "mac1010" AND NOT BINARY_TAG MATCHES "icc" THEN
     valgrind
  ENDIF
)
set(POST_INSTALL ${POST_INSTALL_old})
unset(POST_INSTALL_old)

if (NOT CORAL_lcg_exists AND CORAL_native_version)
LCG_add_test(coral-tests
  ENVIRONMENT BINARY_TAG=${BINARY_TAG}
  TEST_COMMAND ${CORAL_home}/run_nightly_tests_cmake.sh
  LABELS Nightly Release
)
endif()

#---COOL-----------------------------------------------------------------------------------------------
set(POST_INSTALL_old ${POST_INSTALL})
set(POST_INSTALL OFF)
LCGPackage_add(
  COOL
  IF COOL_native_version MATCHES HEAD THEN
    ###SVN_REPOSITORY http://svn.cern.ch/guest/lcgcool/cool/trunk --quiet
    SVN_REPOSITORY svn+ssh://svn.cern.ch/reps/lcgcool/cool/trunk --quiet
  ELSE
    ###URL http://service-spi.web.cern.ch/service-spi/external/tarFiles/COOL-${COOL_native_version}.tar.gz # OLD
    URL http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/COOL-${COOL_native_version}.tar.gz
  ENDIF
  CMAKE_GENERATOR Ninja
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
             -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
             -DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS}
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DCMAKE_VERBOSE_MAKEFILE=ON
             -DBINARY_TAG=${BINARY_TAG}
             -DLCG_VERSION=${LCG_VERSION}
             -DLCG_releases_base=${CMAKE_INSTALL_PREFIX}
             ${Boost_extra_configuration}
             ###-DCMAKE_USE_CCACHE=ON # Can also set it via environment variable
  DEPENDS CORAL ROOT
  IF NOT BINARY_TAG MATCHES "mac" AND NOT BINARY_TAG MATCHES "icc15" THEN
    Qt
  ENDIF
)
set(POST_INSTALL ${POST_INSTALL_old})
unset(POST_INSTALL_old)

if (NOT COOL_lcg_exists AND COOL_native_version)
LCG_add_test(cool-tests
  ENVIRONMENT BINARY_TAG=${BINARY_TAG}
  TEST_COMMAND ${COOL_home}/run_nightly_tests_cmake.sh
  LABELS Nightly Release
)
endif()

#---GaudiDeps------------------------------------------------------------------------------------------
#set(GAUDI-dependencies AIDA Boost Python GSL ROOT QMtest CLHEP HepMC HepPDT RELAX
#                       GCCXML tbb XercesC uuid LCGCMT CppUnit gperftools)
set(GAUDI-alldeps  AIDA Boost Python GSL ROOT COOL CORAL QMtest CLHEP HepMC HepPDT RELAX
                   uuid tbb XercesC LCGCMT CppUnit gperftools GCCXML pytools pygraphics)

set(GAUDI-dependencies)
foreach(dep ${GAUDI-alldeps})
  if(DEFINED ${dep}_native_version)
    set(GAUDI-dependencies ${GAUDI-dependencies} ${dep})
  endif()
endforeach()

foreach(dep ${GAUDI-dependencies})
  if(TARGET install-${dep})
    list(APPEND GAUDI-installs install-${dep})
  endif()
endforeach()

add_custom_target(GAUDI-externals
                 COMMENT "Target to build all externals packages needed by Gaudi"
                 DEPENDS ${GAUDI-dependencies})


#---LCIO-----------------------------------------------------------------------------------------------
LCGPackage_add(
  LCIO
  SVN_REPOSITORY svn://svn.freehep.org/lcio/tags/<LCIO_native_version> --quiet
  UPDATE_COMMAND <VOID>
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DLCIO_GENERATE_HEADERS=off
             -DBUILD_ROOTDICT=ON
             -DROOT_DIR=${ROOT_home}
  BUILD_COMMAND ${MAKE} ROOTSYS=${ROOT_home}
  DEPENDS ROOT
)

#---Geant4---------------------------------------------------------------------------------------------
LCGPackage_add(
  Geant4
#  URL http://geant4.cern.ch/support/source/geant4.${Geant4_native_version}.tar.gz
  URL http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/geant4.${Geant4_native_version}.tar.gz
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DGEANT4_USE_GDML=ON
             -DXERCESC_ROOT_DIR=${XercesC_home}
             -DGEANT4_USE_SYSTEM_CLHEP=OFF
             -DGEANT4_USE_G3TOG4=ON
             -DGEANT4_USE_QT=ON
             IF NOT APPLE THEN
               -DGEANT4_USE_OPENGL_X11=ON
               -DGEANT4_USE_XM=ON
               -DGEANT4_USE_RAYTRACER_X11=ON
             ENDIF
             -DQT_QMAKE_EXECUTABLE=${Qt_home}/bin/qmake
             -DGEANT4_INSTALL_DATA=ON
             -DGEANT4_BUILD_TLS_MODEL=global-dynamic
             -DGEANT4_BUILD_MULTITHREADED=ON
             IF LCG_CPP11 THEN  -DGEANT4_BUILD_CXXSTD=c++11  ENDIF
  BUILD_COMMAND ${MAKE}
  INSTALL_COMMAND make install
  DEPENDS Qt XercesC
)

#---DD4hep-----------------------------------------------------------------------------------------------
LCGPackage_add(
  DD4hep
  IF <VERSION> MATCHES "^v.*-patches|HEAD" THEN
     GIT_REPOSITORY https://github.com/AIDASoft/DD4hep GIT_TAG <VERSION>
     UPDATE_COMMAND <VOID>
   ELSE
      URL http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/DD4hep-${DD4hep_native_version}.tar.gz
   ENDIF

  CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DBOOST_ROOT=${Boost_home}
             -DDD4HEP_USE_CXX14=ON
             -DDD4HEP_USE_XERCESC=ON
             -DXERCESC_ROOT_DIR=${XercesC_home}
             -DROOTSYS=${ROOT_home}
             -DDD4HEP_USE_GEANT4=ON
             ${Boost_extra_configuration}
  DEPENDS ROOT XercesC Geant4 Boost
)

message(WARNING "-DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DBOOST_ROOT=${Boost_home}
             -DDD4HEP_USE_CXX14=ON
             -DDD4HEP_USE_XERCESC=ON
             -DXERCESC_ROOT_DIR=${XercesC_home}
             -DROOTSYS=${ROOT_home}
             -DDD4HEP_USE_GEANT4=ON
             ${Boost_extra_configuration}")

