cmake_minimum_required(VERSION 2.8.5)

# Declare the version of HEP Tools we use
# (must be done before including heptools-common to allow evolution of the
# structure)
set(heptools_version  fnaldb)

include(${CMAKE_CURRENT_LIST_DIR}/heptools-common.cmake)

# please keep alphabetic order and the structure (tabbing).
# it makes it much easier to edit/read this file!


# Externals
LCG_external_package(lcgenv            1.3.2.1                                   )
LCG_external_package(curl              7.48.0                                   )
LCG_external_package(jsoncpp           1.7.2                                    )
LCG_external_package(postgresql        9.5.1                                    )

# Prepare the search paths according to the versions above
LCG_prepare_paths()
